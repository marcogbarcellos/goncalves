$(function(){
$("#elastic_grid_demo").elastic_grid({	
	'hoverDirection': true,
	'hoverDelay': 0,
	'hoverInverse': false,
	'expandingSpeed': 500,
	'expandingHeight': 500,
	'items' :
		[
			{
			'title' : ' 1',
			'thumbnail' : ['https://lh3.googleusercontent.com/IrO9_pNwU8R4a9oJ-7ZS95d7nFsuX_Ywh12pSjgoTa9eGmyVCVoEP_rEburK0HY3JMHGZmk9HcD38P7vWgUfDceHKHEABDayx7HWSTZjoTVLDlGnRATzJsEBYyiQfNDrMI_SdZ9teOWrkyHyVrIouEe_WsyYKtTL-KI4peQk39yje1f1q1rtifVwqWVD9TXbhLRVgmSL3mMqMLBrZYRXQV1rbaGtANowMYO-nq4Goyqa1hXFlOIZ5j47bvU3UPijLfb5h-Gdk-qttF-Q4TUxUiZG6_o11WOHshtL1W9E2cDqwyTrfaJlrlnL35-DZH2eow2BEeSsvb3uf8H_-B2P73wqKpMUCsSsP3B_0nV52uJmlvOnU3cXeB43TbUy7ykg1Ud8b92pVN3wPt_fim2NQgoQVv1gAYteW7LN5d9U-X2ngspucqePn18Heg67FbTq3bvWsRmXjww-fUybqoxU3GjedI3Av2YzSeJf1GYOfl7i3iLj4EcpzbTXSDBrhjE2yV356TuSMVrgGDaB1V_SzItsSxyT4Qnwa-RnWSxRStUNceM20lAMkhkOy-nO3ehBS7S2=w444-h250-no'],
			'large' : ['https://lh3.googleusercontent.com/tz_KE-V207NWnHQDz9cAZHD_wLe79dqcDn90WqDx0INw3Bz6ieS5lxMU1Y8lgdpMVx1obdizHCO39sboMTkysMuesTDH3PuP79MLpXToZkAIJOPhrVdjWpe0mLGq_j5W7ulsrSdUtnUwH7STYYbxOgkeq-uDqW0icjYF-NVC7QMaxMK6hbWCfkP1czKOJylIHv72qoKsm_9tKyVveLNK4lPrLRWASL20CClI-K8fm7E10HEr4_eSC_bfqGK3Oo7ufryDJhM4EGYYg9XOyGCwDDcO_Dppa8ZgKzvqkvI0nBRTtCW-jhiSncTfaRdX5E3XtE6RQvoMdk1bplZY6lXFkB1qKlu35ThRRVvsiZb8oRUESBBfgZmYBPs9q6oI26fJqeUEns2FE_eymSPnFZo5oVr7z-nePDKM6h16Q3hdmDhKkwzYMmAGh3SnZ-TUGR9m9lJbiG5_9uT86oXMUliUfqKa9sz0c_wdpLfVInsI-CQRiNuliQvU8eJGzNdDWyZw7n6JbfKlWfPfD6pYPN7emgKO9-rO5QjHeBCm-EqOeb6wA-FGnaclLXjQCPglOqvQ9ZEU=w550-h309-no'],
			'button_list'   :
			[
			],
			'tags'  : ['Trabalhos']
			},

			{
			'title' : '2',
			'thumbnail' : ['https://lh3.googleusercontent.com/Z1x9b49gf7VcvO70HUEOawP80zCKdv-tkuUWHnb-jRPciJmOiTzT-YKS1WogOg-iQZIJvpaIL_ziC0oaWzCKmnFWQRiD355j1KLNBINkUtkab1oXCzAhBcMXVn7-kmR8TPfScY_w7Wrm3x_GGmgChOkbCwTCclfOU6DhNA7K3wzf5FoD2CJIhGmcufv-8C1A8PmH6QHhGrATif36s8v5U0tShC9jDLRopvdRAM8Gs1Qnq0B6LOWpnkl5kVExXGaWG760TgJMirgRjlpcBQry3lYKS5XT0YZn0-5MlCxWCtBkWVgFQDOZJJuH_poxJsZmwxb1IAGRMsshfDk17VbcR4m0FcR2xuBw1MgHSw9xy7Ph6feqI5RypPnxDcYlwvGTFHzP3IibadmA6A3wWhyelXQG_PWo_zrWiuclecfTQ_iUq4Orcy288jgWHeBbVUh3p28XsiJklM6cAPIm6sBgclJUknRjejncqFFqCa8mwmm6-j61IbW06-C_A9BxpfCxurWqN_LjuwyNJbVdjSyreBAzkqk7unryOfNY8b1sO-6W26fbvgIyvS8kamDQk-I0pXqU=w444-h250-no'],
			'large' : ['https://lh3.googleusercontent.com/xL2MOJuEJA9bnLoZApgxLcs-jAnatq73fagR63VZb66eqWGSIEc3M0OsFwakfAmttyx_7_3-dQkIUNbS0wAmx83Bwli1f9Dd-ubtDLf3ImXVO7Cv91VnI6B2fJs7UNA_yu9t11ueSwA59w7jRsWY5A8XCazys-lmnZZBr5TfeqMbf4K5CZoRowOaKwahcW2m86QhVR4DRZIlHUczKXRCkKEchn6lWbPk0RvF3GbUvBqBivYVnGIStR-xjVH9O7WB4YKcDTXyxtNdYNEw2UkSSMgF6e-J7if-vMULGZKKfZ9PerP2S0lh5VJzXk3KCCfWp58P61n1o_fDvjJ9ebqc93l4ZpaPBnzKtudXLGikN5JluES_GPxDfmsGcCHjos1jJ8Ax_6N-s-sXX3bLu5VgTiNir8uqmrg3XJTu3vl9urksHbxnvFx826XzKkcsKJYypCKEQOeFMWdJAtJpAFWk4Vvcu95nPNfaqmd8yagdMP8vIuBvH9VCL52eSnElpXRgzFA58m0hTdBByEAh8xMC_dsGPbFENIRZYSwr5zdEiwhzE_v9Ei3cCv6hiH2QHBGfcN5l=w550-h309-no'],
			'button_list'   :
			[
			],
			'tags'  : ['Trabalhos']
			},

			{
			'title' : '3',
			'thumbnail' : ['https://lh3.googleusercontent.com/PJse16XPQ9JQpIbtgeLKKbvJrf1ZedJ8si9rf_o3fQsVLknFipDSUfIfeaFF0cDQO6rDlejqn82pKv6TSY1utdYScZuzEdrihunrBPf8hgbVg0WJEtBSF7WrXg44oP3BSYDTeADbysGXumMEVe-9rSn5JgZ6qON7q_mbt3g4hobaItBqhEv2cR-f07dGGVP2fY_5p04dNGFfDotaprCH563RqeR6Gr2mf9D-pbbJ-k9sMlR7xW7z2QEnYc0J_x683t0A-QyQOVy0hO9zVxyokrVa7qMuQ3nC4HhEKklZJKkFHAiNno5BW4tLuY2ld7lwsb3wQEPiSn3SR5Z7UWVlneKm_zQEmVWj_XBfRA9K0mPkMjm1HpxvgE9E7xgNYs9CMj6ypq1H6n-zpBGEPwmW31O7JixwwAQQ5PcH1CCfreS2t_AjO8Tohfs2d3Mk6reiEqaEdBGN6wPRRgrOeFiVPcMYqTeNtb4JwegIfvD2fvojFyBMWumNgbRYLIdynRz1G16UNcrRNa5dwga4VGRkeGPT4l0PS2oyii0UMT97X2fnpmIM3bFz5bmx9UMCcKoTojeq=w444-h250-no'],
			'large' : ['https://lh3.googleusercontent.com/TCs2F0PcigWZe6R83ntnn69Kq_MJBLYEGBiouNu0MDZ33CjHshWlnjWZOWJ0UB7OXICgXMadw6OKA-Fy3aTRo1xcu6DPhBVf8EkQJqPqRzybDBmPkMQZ0lApR82plqCSUy4_7InhUY2XXdGxYVVHJRqx2RNrsYQqcKnFrGWAwf_MzQ6CXXuqtxpTixzydWVnKb58vgv2qeZgnxHvGdug7AF69F4xDZqLBrdaSajT3lf6-0tBMQEdUoGtxszuEHAxf6cUsiVkRiE-SfcKOc7dwsYpgHANR3TMesDy6NSsovn2cYAT9PewrUXpn6QFGexIczRxKk1QZZHe4M2j1q8m14E8K47BunEqhJQ56fhpjRXCyCi-zAsvEp6Do4sEYLVIvibRUpRReuZ0t9FvsIjgsYP-qgkTmMlzyM2cZaUVaffJATOeaz0WU1B0Rju3dVTz_SnctH8L0nsxEeP49Bg1nOiZXjXh_mbspFkDpnH-qruAQ4BYq-RlzcN6DhJ6UhWrmPKYHGeH3e8VjA0J3Lfr4vyUfXcb-wQFqA9TXilkmwa6UDrmFGg8tiu1p0Lx-WaiZELI=w550-h309-no'],
			'button_list'   :
			[
			],
			'tags'  : ['Trabalhos']
			},

			{
			'title' : '4',
			'thumbnail' : ['https://lh3.googleusercontent.com/esQZJh2Fzqd887dnZuTWSdd3CweH6neO7Md1JBIRV3KD476ocD6g-zw2WcwjSpgY92x61sjECvvgmIDjmyuxaSSeERxexRtRwIoRDwVG3fBIHyZpgZx_9hgIspBS86fT_tKomSfE7XTrBYRemDgpB86ZyfYxENZ_YJKM3ZP9SbKIJ14-72Cejl-ssOC2BCSRsDFkOxEOvbXb4LC2A78sRCFiSwYFuHwYEhLRFK-sGFB6ru-pmrlT2Sl4JG0G1NHVTFwRI0y8cGKAR33PuFFpcd6ov6DeBU87CujCimc35uqD8Y4vGfEcfdgZQ6LI8G4JTO2mnQ0Rd9qnBQP_U-tDQWGlLSA0FDGSCvmZ3MYzttRUZZX2ZjfSENig91t5XNIyCpA90FembrA8lWTDwPrzYkC11hjwpLPxvgINJAigEBtYVlteHaMc0ebWdDU2ep09H2V3dk8UQHrcFEHE9hyGvkksN8duv3AInWJxrH_9zlsMfmaEBhhOhsSzxSA_bIi6hWdrOLcwpFLvWkHeEwHBsDAo0my9nLK6h7t6TrHbQEO2mYoqaMV4qaaBD5AAjZUnhZVd=w444-h250-no'],
			'large' : ['https://lh3.googleusercontent.com/gSsXFgzxeert5a4T03LdPFH2lMtffG21H2SKGO0tOgvvWNwOV0fq0uSVLXknXOegbEeNqJI5024CHhGr77SmHunWkknfx33VSIqFU1M2hmPOk1xBDy2UL5RyTKSfZi4OiXgO9drk3oJOvAMmA9Z0E7-qcvUW21y7Y1vpdzyPMnzzYnpdzjTK4QTe296UHpk3FMyB6o2Yo_jczp0-mj8bR049nqXtlECJ45f_mpFyY-6hNTYKH999alRsEm4KlP7KsIhbbRqMXYE2bOAGKg3-h_w_JPGGnKlOtC0IE3J5UyRveoYP3b3RS1EO1Gf4-iKQOHjrfaWIcyMa0UNJtu2K6wxZ8WWAJ5J_xaltXzaIAa5uj9M0ICaFs2xgCS-JLh7iEFoKBroAgew5G0uo9uVFBJhOjcOd1TazH2smlRpEUAnmohkPLvMQrdu10DNL6M9XI08ZhGfWkhjAsmFHUq0aJgozB_AJ2U-rDPaDasat8pN-Pyah4DIAP27iqxQqyexKw_2s-ZAErZ85ONh0l62BZC-IEYcjHmV1CvliTjgggfL2qlUtpH40Wui1EggxeD4mMZaR=w550-h309-no'],
			'button_list'   :
			[
			],
			'tags'  : ['Trabalhos']
			},

			{
			'title' : '5',
			'thumbnail' : ['https://lh3.googleusercontent.com/QeOYOPj-h6rpL4ADa0oQ0xTQ965IC2GqrYbxVIa8JuuVjKzJYAxT6VKrljN_o6Rpu0HmSHc0GaH5eD6grGEAbiLaXy5CU-t9F3igpuaZU-ffDPzHAQWySkRhe80W2eHfOKLSWvTAfkM9a0BevFOhIx-8Mzhtwc29lTEDm3La0qdyhu_F7xvNjZ9Z_fHyEOUNHfPJ-vU6LxDf_si6IOaQAg4Vx_TpL2-EeB-gJIEQvlKImrZHjOO5wDbHFiPjsAWrar9lwgHzjfWgfVnRTZTkU7cQtx7p7ZfRrjdTwywuWaepAmfADKWFk9FhNxhdyu0zjOGP-Lu2uH6ct7j38LembRuNTCMyg37IlKcVFyUKUmxz9debrHAoEsrRK51XsFoJSpGq89IKCT39u2LKOd9QqIfYt9YT0_IOMYn1TgbcJcEOvwr6SHMPuvYXCRoAUwqn4J7JDnIPsq2klGaCux9-4tCf7E2zzjmbev1m7RYYRNSeSf0B78f_IqFwBMCDj2PcE2rz4I85ORAc0zukYIHf5VTdXjjCFs94h-Jxi9MnF3QCsPEwbnyHZT5nDe3HxY2dLIjA=w444-h250-no'],
			'large' : ['https://lh3.googleusercontent.com/t8THyOBxflRU5JZ4wOL4tNlJf_Anvx-hWVycdCIkVUHTB26SvpsBrcA02DLdA18n6KS6vovIzt3AQ0yA9G3P8wsxeLrVf_NmWPyJOg_MkVec-Q-HLonoPu6HyW0X1e4lI4lcFIelO8abBojHAeSPH46Fv3KaOYi7YGPaue4lcXyLYlAe3zqWtUUO1bopgtxzYQr_8oW1Ud-5-UjmkwgJLVIC3sEA99xE7FjcQdWKxhGptzOgKeVs2F-VGe7AUIW3DBiVt7PL5mxQj5o4mKd63XqOq_WSyn6VWMeleA98rwsSxvRjqtVvH9QGcW7PZ4-PQjxjCmnoOKAUOULZfO-ZzCz0TO884SOfGfhSqWMapxFtn8bhUsPl11saRrw9ix8j3HwgtcWkJNhOk2VlPVOoPJyy2d3PQpBY-2mD2gn6qrOeOA8S0vycVLpQZLTu1E7ROlQ7zqi3a_EK4JKfL-Sk-7SBXmjJpP5knHHnCRCe7Cu_bjSjDW6wQ29SrdTU1rDPeStEsCXeB2oUkfl9kX6V33KzLMG3xy-kqJTFRMp9mis6CeJgGCfMPH1U-SSDAp56aTj6=w550-h309-no'],
			'button_list'   :
			[
			],
			'tags'  : ['Trabalhos']
			},

			{
			'title' : '6',
			'thumbnail' : ['https://lh3.googleusercontent.com/fF_wrtIjYDLFTYiVEmvNGA70nC_DTDbkrKLr4AKPv8sLkFNiUJ-Nuy6JsARLq3rqwnbCfjIpY-FgOFjKnL7O3m4oKFTB76BrM6JoC_FcDFuhehPzWGWGIOdznk4R4JsPDQjhAcWaKhWQ8pIP5tDgtNArMzjgwLsHX67WdwdzA--G_ZX_DSlA9HY9GZYRUWLp09M2LxUoyeTyhZDTGUQ32sx6zdGCZnQJ5E-XopAoCbt_BsGiz-E0zhfM7IG-abzU15eP293RBeiGhqv5S7WexpOijnFU8i08hPI3fxpx8WMyrOP1lfDQPfUExLTciQUqNThPjlJZ41bf-fTVPyYubDtded5-zyaSJnAKShpibSrqY6YstJBNyw9fM5cPPy0iVUkkuAlJ_RQcwIOf1ev8ljVxZFLaIy_G4uAmKhpfONZI0zc3Y66qolLECGzRYj4U5pVEV-ypsKadT4Iog0oagLHlCzJX5LLrxMvZANxeBepZlTu2hVCp6_eKZkUubd7axUc6_D6bWA8FKcKW2nYuw3d0qnzWmQ7G6wEQR3Ozlh2HzsyRJJNLqzhkC6yhglo0fOXy=w225-h250-no'],
			'large' : ['https://lh3.googleusercontent.com/p5gKD4rkkD3S8R9RR854V1JBcOGLjOtFdl5PbTC6WE1KcCQh9c73boQrpmiCX-a-TDwScH2ZdlEJGRrIBvNtNfRGMlCYdVv2EY76i-KjgS88BmRbvaFoxd9SRRSL0FGSQjQ5HhVu9c8nGA2s_svUxQYcA0y48iLNGkd4soAjfvCvf7G92CSuuS6sKU4eLhz-rVXnm7ozZNIClsFtsKJ53gyM95KicOapGGGA707eb2FB-9Hra7bP0AmIB7BPiZKh_X_NehI2Rx1H7PrNMZr85jMPQyPB6b2EdMgvNZ6abe7hipbjboLkFE2PVvoM5WkapIcnfKrWHYIMDVbD9SalU3LDu4CMCNcWTzLb-0VGsRS0rU7uzq07p6geAfJJD8VOIcEEoiUNFIv1ICkFuDcSpG4yAEegGGRpe4d5eMW5CabA3X2dJvz317bWuenUPwx_w6YaAgEv0laqWLiyQW7sG3Fs_Ct7JHZwfsD6x4IICkYvRVEo9hGpIPeFMvY4zCPoAG8rWxs3w4zuixuYlblmO9ZDUUaHXMxoBbyzVCoqHnKEkpUP-SR9HleLTzzYnQx0FXvf=w371-h659-no'],
			'button_list'   :
			[
			],
			'tags'  : ['Trabalhos']
			},

			{
			'title' : '7',
			'thumbnail' : ['https://lh3.googleusercontent.com/jnvuW_BhCEIf79Trr6tCJNliwMfS73NvSHBTRL9hMsh3NdoNDF2TzQ5JKBbM7oeJR6GplrmCLIZy-JloXOum9zakSp0B0MbWLjQioRWSx9caKMVhuSP2J-KlWT4nYUNBDqtHTW98N0wWymK3_LRtxZs6ZQCyKdVaLP0uCJZ7B_6YhEcwvFg6QBFtDNOXN9IJpaAo4BOTzv03bUvz4B8-aFpKWV-9R3AthbO2Cox2-hDYHMVkPHh5P6Kb8EtRrc1PGXLThx1Zl7SQKXIUs50O8Y0HqGEtMImsuXuBMU0Bd9O9PtS8NRLPlOvzMmG5Go6pHzZ17UWU1i-nDv1_tkAr0IumO5stCGIhmIYl2v6sb6mChEUimF_RtaL-sLsg8bMKV3Y9pD34Ye70dhxmrxVYylydVsjBclpEC0xy899oidhpMJ0JuSqUGIIkb2y8Xqy67eQULLbvpehrk4RFdd1DjFXMSKjocnMOroBUQnwqvIFCbuaTdAIJCjnxWz1Q-whOz0_kiKaP5U400SGejb4Dncyr4MVfYyFh6OVsRnxBQc4xYPLAOjnqKuj5r_nUOjtT9_yJ=w239-h250-no'],
			'large' : ['https://lh3.googleusercontent.com/UyrLjvXdEw7Ed75F6ct7Mc-omOMQpZayWWhEu8Z5kF1D5NK4aW0J8Aj-rmP4KZI7-extdrYCCbB11U7oRiJiixBu_DEw5vtKbmnWFN5Ewf-gH1A_MrFhCvkUlX0Z2JpDjbhbLW6oHTKbT03r1rgwEMibKpcJoHnGAarLXvMYXAyNDZS1CFtoJvCXtct-hz8GFvP4il-CH-aMQLizu-cQyDHYyeNHsmtecI1A0GrCFsdjx25b3JyUDUolN6iDaPUNQb0TJSpwc4trcJaFuXLqKzyJ5YWkloD0b7_pW4JRBgvDLsbQYQVcIE9652XcvM_R895CVYbF6twJaVNt_2BX8878_YgyGHIlzyc3yVscDYUxgVDnsWlxXlLGOQPeDMhvPo20ddxJ0P3x89aOHwOZZpx79M9oGqNWhXrfon6GKnfUkZagMmP3KVNttnclaN_DDUTEyp_4PNeODrLpyIhaU6g25_GUSdNkTnLhJJ9_E5ygrKmXET0D9bm90SdJJwyeh4AWi8NeMfSsNOQmTsq_M_XnZqEER_xtrVzVSTtsNWVjMxqItfyCa3wnzFuntaCjVahC=w371-h659-no'],
			'button_list'   :
			[
			],
			'tags'  : ['Trabalhos']
			}			
		]
	});
});